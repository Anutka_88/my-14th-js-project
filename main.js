document.addEventListener("DOMContentLoaded", ()=>{
    init()
  })
  function init() {
    if(localStorage.getItem('theme')) {
      document.documentElement.setAttribute("theme", "other");
    }
    else {
      document.documentElement.removeAttribute("theme");
    }
    
  }
  const toggleBtn = document.querySelector("#toggle-theme");
  toggleBtn.addEventListener("click", function() {
    if(document.documentElement.hasAttribute("theme")){
      document.documentElement.removeAttribute("theme");
      localStorage.removeItem('theme');
    }
    else{
      document.documentElement.setAttribute("theme", "other");
      localStorage.setItem('theme', 1);
    }
  });